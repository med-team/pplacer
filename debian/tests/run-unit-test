#!/bin/bash
set -ex

pkg=pplacer
CUR_DIR=`pwd`

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp ${CUR_DIR}/debian-tests-data/* -a "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

echo "127b8f054fc0e2cade75c7922f06e919  p4z1r36.xml" >> checksums
echo "eb8193d86364a6c5687199f4cd1b2caf  dist_matrix" >> checksums
echo "0e738fe4fccf66d29c349af393393e8b  cluster.tre" >> checksums
echo "e7ccd9be33592127b60649b122d017fe  pca_out.xml" >> checksums

echo -e "\e[93m\e[1mRunning Tests\e[0m"  #Running demo commands

#Phylogenetic placement
pplacer -c vaginal_16s.refpkg src/p4z1r36.fasta

#xml fat tree visualization
guppy fat -c vaginal_16s.refpkg p4z1r36.jplace

#statistical comparison
guppy kr src/*.jplace > dist_matrix

#edge principal components analysis
guppy epca --prefix pca_out -c vaginal_16s.refpkg src/*.jplace

#squash clustering
guppy squash -c vaginal_16s.refpkg --out-dir . src/*.jplace

if [ "$(uname -m)" = "x86_64" ]
then
	# Implement strict checking for results on amd64.
	du p4z1r36.xml pca_out.xml  # XXX getting reference file size.
	md5sum --check checksums
else
	# On other architecture, visual inspection suggested some drifting in
	# floating point values, but they tended to remain within "reasonable"
	# margins.  Since the XML does not really make comparisons easy without
	# having to spawn a parser and compare each floating point value and
	# compare to a reference, we just make sure the test results in an XML
	# that has a certain size within a certain margin.
	file "p4z1r36.xml" | grep 'XML 1.0 document'
	file "pca_out.xml" | grep 'XML 1.0 document'
	readonly p4z1r36_size="$(du p4z1r36.xml | cut -f1)" # expect 176 k
	readonly pca_out_size="$(du pca_out.xml | cut -f1)" # expect 588 k
	test "$p4z1r36_size" -ge 128
	test "$p4z1r36_size" -le 224
	test "$pca_out_size" -ge 540
	test "$pca_out_size" -le 636
fi
echo -e "\e[92m\e[1mPASS\e[0m"
